package dev.ccuevas.recipebook.disheslist

import dev.ccuevas.recipebook.RecipeBook
import dev.ccuevas.recipebook.RecipeBookImpl
import dev.ccuevas.recipebook.SelectDishesInteractor
import dev.ccuevas.recipebook.models.Dish
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SelectDishesUseCaseUnitTest {

    @Mock
    private lateinit var mRecipeBook: RecipeBook

    private lateinit var mSelectDishesInteractor: SelectDishesInteractor

    private val mExpectedDishesListResult = listOf(
        Dish("egg sandwish", listOf("eggs", "bread")),
        Dish("bowl of cereal", listOf("milk", "cereal"))
    )
    private val mIngredientList = listOf("eggs", "bread", "cheese", "milk", "cereal")

    @Test
    fun dish_list_search_successful() {

        `when`(mRecipeBook.getDishesWithIngredients(mIngredientList))
            .thenReturn(mExpectedDishesListResult)

        mSelectDishesInteractor = SelectDishesInteractor(mRecipeBook)
        val result: List<Dish> =
            mSelectDishesInteractor.searchDishesWithIngredients(mIngredientList)

        assertEquals(result.size, mExpectedDishesListResult.size)
        assertThat(result, `is`(mExpectedDishesListResult))
    }
}