package dev.ccuevas.recipebook


import dev.ccuevas.recipebook.disheslist.SelectDishesUseCaseUnitTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(SelectDishesUseCaseUnitTest::class)
class RecipeBookTestSuit