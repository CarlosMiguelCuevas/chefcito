package dev.ccuevas.recipebook

import dev.ccuevas.recipebook.models.Dish

class SelectDishesInteractor(val recipeBook: RecipeBook) {

    fun searchDishesWithIngredients(ingredientList: List<String>): List<Dish> {
        return recipeBook.getDishesWithIngredients(ingredientList)
    }
}
