package dev.ccuevas.recipebook

import dev.ccuevas.recipebook.models.Dish

interface RecipeBook {
    fun getDishesWithIngredients(ingredientList: List<String>): List<Dish>
}