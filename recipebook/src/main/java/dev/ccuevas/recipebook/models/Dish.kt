package dev.ccuevas.recipebook.models

data class Dish(val name: String, val ingredients:List<String>)